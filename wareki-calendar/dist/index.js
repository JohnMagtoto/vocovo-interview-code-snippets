"use strict";
/**
 * Wareki Year Converter
 *
 * By JohnMagtoto <johnmagtoto@gmail.com>
 */
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
/**
 * Enum year string values
 */
var WarekiYears;
(function (WarekiYears) {
    WarekiYears["REIWA"] = "\u4EE4\u548C";
    WarekiYears["HEISEI"] = "\u5E73\u6210";
    WarekiYears["SHOWA"] = "\u662D\u548C";
    WarekiYears["TAISHOU"] = "\u5927\u6B63";
    WarekiYears["MEIJI"] = "\u660E\u6CBB";
})(WarekiYears || (WarekiYears = {}));
/**
 * Gregorian Calendar year to Japanese Calendar year (Wareki) converter
 */
var WarekiYearConverter = /** @class */ (function () {
    function WarekiYearConverter() {
        /**
         * Current era adjustment
         */
        this.currentEraYearAdjustment = 2;
    }
    /**
     * Convert Gregorian Calendar year to Wareki year
     * @param {string} year The year to convert
     */
    WarekiYearConverter.prototype.convertToWarekiYear = function (year) {
        // Check for supported types
        if (typeof year === 'string') {
            if (year.length > 4 || isNaN(parseInt(year))) {
                console.error("Passed string value is not a proper year value: " + year);
                return null;
            }
            // Check if year has isValid marker to identify if passed value is a moment
        }
        else if (!moment(year).isValid()) {
            console.error("Cannot process passed year value! Must pass year of type string or moment object");
            return null;
        }
        /**
         * Make sure that the passed year is converted
         * to moment for convenience
         */
        var yearAsMoment = this._convertYearAsMoment(year);
        if (!yearAsMoment.isValid()) {
            console.error("Passed year, " + year + ", cannot be converted to moment");
            return null;
        }
        switch (true) {
            case yearAsMoment.isAfter(this._convertYearAsMoment('2019'), 'year'):
                return this._findReiwaYears(yearAsMoment);
            case yearAsMoment.isSame(this._convertYearAsMoment('2019'), 'year'):
                return '平成31年/令和1年';
            case yearAsMoment.isBetween(this._convertYearAsMoment('1990'), this._convertYearAsMoment('2018'), 'year', '[]'):
                return this._findHeiseiYears(yearAsMoment);
            case yearAsMoment.isSame(this._convertYearAsMoment('1989'), 'year'):
                return '昭和64年/平成1年';
            case yearAsMoment.isBetween(this._convertYearAsMoment('1927'), this._convertYearAsMoment('1988'), 'year', '[]'):
                return this._findShowaYears(yearAsMoment);
            case yearAsMoment.isSame(this._convertYearAsMoment('1926'), 'year'):
                return '大正15年/昭和1年';
            case yearAsMoment.isBetween(this._convertYearAsMoment('1913'), this._convertYearAsMoment('1925'), 'year', '[]'):
                return this._findTaishouYears(yearAsMoment);
            case yearAsMoment.isSame(this._convertYearAsMoment('1912'), 'year'):
                return '明治45年/大正1年';
            case yearAsMoment.isBetween(this._convertYearAsMoment('1869'), this._convertYearAsMoment('1911'), 'year', '[]'):
                return this._findMeijiYears(yearAsMoment);
            case yearAsMoment.isSame(this._convertYearAsMoment('1868'), 'year'):
                return '明治1年';
            default:
                console.error("Passed year, " + yearAsMoment.year() + ", is out of bounds!");
                return null;
        }
    };
    WarekiYearConverter.prototype._findReiwaYears = function (yearAsMoment) {
        var yearEndToUse = moment().year();
        if (yearEndToUse === 2019) {
            yearEndToUse += this.currentEraYearAdjustment;
        }
        return this._yearMatcher(WarekiYears.REIWA, 2020, yearEndToUse, yearAsMoment.year());
    };
    /**
     * Find Heisei equivalent
     * @param {Moment} yearAsMoment The year to convert in Moment form
     */
    WarekiYearConverter.prototype._findHeiseiYears = function (yearAsMoment) {
        return this._yearMatcher(WarekiYears.HEISEI, 1990, 2018, yearAsMoment.year());
    };
    /**
     * Find Showa equivalent
     * @param {Moment} yearAsMoment The year to convert in Moment form
     */
    WarekiYearConverter.prototype._findShowaYears = function (yearAsMoment) {
        return this._yearMatcher(WarekiYears.SHOWA, 1927, 1988, yearAsMoment.year());
    };
    WarekiYearConverter.prototype._findTaishouYears = function (yearAsMoment) {
        return this._yearMatcher(WarekiYears.TAISHOU, 1913, 1925, yearAsMoment.year());
    };
    WarekiYearConverter.prototype._findMeijiYears = function (yearAsMoment) {
        return this._yearMatcher(WarekiYears.MEIJI, 1869, 1911, yearAsMoment.year());
    };
    /**
     * The year matcher core
     * @param {string} warekiText The wareki year
     * @param {number} yearStart The start year range
     * @param {number} yearEnd The end year range
     * @param {number} yearToMatch The year to match/find
     */
    WarekiYearConverter.prototype._yearMatcher = function (warekiText, yearStart, yearEnd, yearToMatch) {
        // Special condition for current Japanese Era:
        // Assume this is year for all years unless new era is declared
        if (warekiText === WarekiYears.REIWA && yearToMatch > yearStart) {
            return warekiText.concat((yearToMatch -
                yearStart +
                this.currentEraYearAdjustment).toString(), '年');
        }
        for (var i = this.currentEraYearAdjustment, yearMatch = yearStart; yearMatch <= yearEnd; i++, yearMatch++) {
            if (yearToMatch === yearMatch) {
                return warekiText.concat(i.toString(), '年');
            }
        }
        console.error("Year " + yearToMatch + " not found");
        return null;
    };
    /**
     * Convert a year or moment-type value to a year-centric moment-type
     * @param yearValue The year value to convert
     */
    WarekiYearConverter.prototype._convertYearAsMoment = function (yearValue) {
        if (typeof yearValue === 'string') {
            yearValue = this._padMonthAndDay(yearValue);
        }
        return moment(yearValue, 'YYYY-MM-DD');
    };
    /**
     * Pads the year with a month and day to be acceptable to moment library
     * Conforms to ISO8601
     * @param year The year to pad with month and day
     */
    WarekiYearConverter.prototype._padMonthAndDay = function (year) {
        return year.concat('-01-01');
    };
    return WarekiYearConverter;
}());
exports.WarekiYearConverter = WarekiYearConverter;
/**
 * Convert Gregorian Calendar year to Japanese Calendar year (Wareki)
 * @param {string} year The year to convert
 */
function convertToWarekiYear(year) {
    var wyc = new WarekiYearConverter();
    return wyc.convertToWarekiYear(year);
}
exports.convertToWarekiYear = convertToWarekiYear;
//# sourceMappingURL=index.js.map