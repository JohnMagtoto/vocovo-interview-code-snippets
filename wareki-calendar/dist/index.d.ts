/**
 * Wareki Year Converter
 *
 * By JohnMagtoto <johnmagtoto@gmail.com>
 */
import { Moment } from 'moment';
/**
 * Gregorian Calendar year to Japanese Calendar year (Wareki) converter
 */
export declare class WarekiYearConverter {
    /**
     * Current era adjustment
     */
    private readonly currentEraYearAdjustment;
    /**
     * Convert Gregorian Calendar year to Wareki year
     * @param {string} year The year to convert
     */
    convertToWarekiYear(year: string | number | Moment): string | null;
    private _findReiwaYears;
    /**
     * Find Heisei equivalent
     * @param {Moment} yearAsMoment The year to convert in Moment form
     */
    private _findHeiseiYears;
    /**
     * Find Showa equivalent
     * @param {Moment} yearAsMoment The year to convert in Moment form
     */
    private _findShowaYears;
    private _findTaishouYears;
    private _findMeijiYears;
    /**
     * The year matcher core
     * @param {string} warekiText The wareki year
     * @param {number} yearStart The start year range
     * @param {number} yearEnd The end year range
     * @param {number} yearToMatch The year to match/find
     */
    private _yearMatcher;
    /**
     * Convert a year or moment-type value to a year-centric moment-type
     * @param yearValue The year value to convert
     */
    private _convertYearAsMoment;
    /**
     * Pads the year with a month and day to be acceptable to moment library
     * Conforms to ISO8601
     * @param year The year to pad with month and day
     */
    private _padMonthAndDay;
}
/**
 * Convert Gregorian Calendar year to Japanese Calendar year (Wareki)
 * @param {string} year The year to convert
 */
export declare function convertToWarekiYear(year: string | number | Moment): string | null;
//# sourceMappingURL=index.d.ts.map