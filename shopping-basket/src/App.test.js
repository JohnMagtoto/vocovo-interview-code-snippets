import { fireEvent, render, screen } from "@testing-library/react";
import App from "./App";

// User can see the basket and contents
test("renders the basket and contents", () => {
    render(<App />);
    const basketContainer = screen.getByTestId(/basket/i);
    expect(basketContainer).toBeInTheDocument();
    const basketItems = screen.getAllByTestId(/^cart-item$/i);
    expect(basketItems.length).toEqual(3);
});

// User can update item cost by changing quantity
test("able to change item total cost by updating quantity", () => {
    render(<App />);
    // Get the first item in the cart (price: 1.8)
    const basketItem1 = screen.getAllByTestId(/(^cart-input$)/i)[0];
    fireEvent.change(basketItem1, { target: { value: "2" } });
    expect(screen.getByDisplayValue("2") === basketItem1).toBe(true);
    const cost = screen.getAllByTestId(/^cart-item-cost$/i)[0];
    expect(cost.textContent).toEqual("$" + (1.8 * 2).toFixed(2));
});

// User can see the total cost
test("able to see the total cost of the cart", () => {
    render(<App />);
    const cartItems = screen.getAllByTestId(/(cart-input)/i);
    fireEvent.change(cartItems[0], { target: { value: "1" } });
    fireEvent.change(cartItems[1], { target: { value: "1" } });
    fireEvent.change(cartItems[2], { target: { value: "1" } });
    const total = screen.getByTestId(/^cart-total$/i);
    expect(total.textContent).toEqual("Total: $7.70");
});

// User can clear the items from the store
test("able to clear items from cart", () => {
    render(<App />);
    // Pre-fill 1 cart item
    const basketItem1 = screen.getAllByTestId(/(cart-input)/i)[0];
    fireEvent.change(basketItem1, { target: { value: "1" } });
    const clearButton = screen.getByText("Clear");
    expect(clearButton).toBeInTheDocument();
    fireEvent.click(clearButton);
    const total = screen.getByTestId(/^cart-total$/i);
    expect(total.textContent).toEqual("Total: $0.00");
    const cost = screen.getAllByTestId(/^cart-item-cost$/i)[0];
    expect(cost.textContent).toEqual("$0.00");
});

// User can pay for the items they have in the basket
