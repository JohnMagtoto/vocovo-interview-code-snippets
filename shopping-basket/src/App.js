import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import "./App.css";

const productData = [
    {
        id: 0,
        name: "Mountain Dew",
        price: 1.8,
    },
    {
        id: 1,
        name: "Desperados",
        price: 2.58,
    },
    {
        id: 3,
        name: "Jack Daniels",
        price: 3.35,
    },
];

/**
 * The product state hook
 * NOTE: Currently behaves as product data interface to wrap unknown data source
 * @returns The products state wrapped in an array (useState tuple, 1st part)
 */
function useProductState() {
    const [products] = useState(productData);

    return [products];
}

/**
 * The cart state hook
 * @param {Product[]} data The array of product data
 * @returns A tuple containing the cart state and cart update method
 */
function useCartState(data) {
    const [cart, updateCart] = useState([]);

    useEffect(() => {
        if (!cart.length) {
            updateCart(
                data.map((item) => {
                    item.count = 1;
                    return item;
                })
            );
        }
    }, [data, cart]);

    return [cart, updateCart];
}

/**
 * The interactable shopping cart component
 * 
 * This shopping cart component is implemented with the following caveats and assumptions:
 * - User already added 1 unit per cart item in the shopping cart upon initial display
 * - The price of each item is obtained by using the sample image in project's readme
 *   - COST / ITEM_COUNT = ITEM_PRICE
 * - It is assumed that the cost per item is rounded to the nearest first decimal figure
 *   - In the included example, `Desperados` cost was computed to be around $2.58
 *   - Computed value of 6 `Desperados` should be about $15.48 but was $15.50 in the example
 * 
 * @returns The shopping cart component
 */
function App() {
    /**
     * The hook variables that define component state
     */
    const [products] = useProductState();
    const [cart, updateCart] = useCartState(products);

    /**
     * Clean the numeric input as string to prevent non-numerical values inputed through the keyboard
     * @param {string} newVal The new numeric value to clean
     * @param {number} oldVal The previous numeric value
     * @returns The new numeric value
     */
    function sanitizeNumericInput(newVal, oldVal) {
        return newVal.replace(/\D/, "");
    }

    /**
     * Round of number to the 1 decimal place with 2nd decimal with a trailing 0
     * @param {number} num The number to round off
     * @returns
     */
    function roundNumber(num) {
        return (Math.round(num * 10) / 10).toFixed(2);
    }

    function computeCartTotal() {
        const total = cart.reduce((total, item) => {
            return item.count ? total + item.price * item.count : total;
        }, 0);

        return roundNumber(total);
    }

    function clearCartItems() {
        updateCart(
            cart.map((item) => {
                // Setting the item count of each item to 0 clears individual
                // cart item cost and total cost as these are derived values
                item.count = 0;
                return item;
            })
        );
    }

    function renderCartItem(item) {
        return (
            <div
                aria-label="cart item"
                key={item.id}
                className="cart-item"
                data-testid="cart-item"
            >
                <span className="cart-item-name">
                    <label htmlFor={`${item.id}-count`}>{item.name}</label>
                </span>
                <span>
                    <input
                        id={`${item.id}-count`}
                        data-testid="cart-input"
                        type="number"
                        min={0}
                        size={3}
                        value={item.count}
                        onChange={(ev) => {
                            const countAsNum = sanitizeNumericInput(
                                ev.target.value,
                                item.count
                            );
                            item.count = countAsNum;
                            updateCart([...cart]);
                        }}
                        onBlur={(ev) => {
                            // Set value of item to 0 when user focus leaves the input field
                            // with empty value (`""`)
                            if (ev.target.value === "") {
                                item.count = 0;
                                updateCart([...cart]);
                            }
                        }}
                    />
                </span>
                <span className="cart-item-cost" data-testid="cart-item-cost">
                    ${roundNumber(item.count ? item.count * item.price : 0)}
                </span>
                <span className="cart-item-control">
                    <button aria-label="remove cart item">
                        <FontAwesomeIcon icon={faXmark} />
                    </button>
                </span>
            </div>
        );
    }

    return (
        <main className="App">
            <div className="cart" data-testid="basket">
                {cart.map((item) => renderCartItem(item))}
            </div>
            <div className="cart-controls">
                <span className="cart-total" data-testid="cart-total">
                    {`Total: $${computeCartTotal()}`}
                </span>
                <span className="cart-control-buttons">
                    <span>
                        <button
                            data-testid="clear-cart"
                            onClick={clearCartItems}
                        >
                            Clear
                        </button>
                    </span>
                    <span>
                        <button data-testid="checkout-cart">
                            Check Out <FontAwesomeIcon icon={faAngleRight} />
                        </button>
                    </span>
                </span>
            </div>
        </main>
    );
}

export default App;
