import { Pool } from "pg";
import { getLogger } from "../common";

// TODO: transfer to environment config file
export const SESSION_LIFETIME = process.env.SESSION_LIFETIME;
export const COLLAB_SESSION_LIFETIME = process.env.COLLAB_SESSION_LIFETIME;

let mainPool: Pool | null;

function createPool() {
    const port = process.env.POSTGRES_PORT || "5432";
    return new Pool({
        user: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD,
        host: process.env.POSTGRES_HOST,
        port: port ? parseInt(port),
        database: process.env.POSTGRES_NAME,
    });
}

export function getPool() {
    if (!mainPool) {
        mainPool = createPool();
    }
    return mainPool;
}

export function initializeDBPool() {
    const pool = getPool();

    pool.on("error", (err) => {
        getLogger().error("Unexpected error on pool", err);
        process.exit(-1);
    });
}
