import { Request, Response } from "express";
import moment from "moment";
import { getLogger } from "../common";
import {
    SOCKETIO,
    SOCKETIO_EVENTS,
    SocketIO_Notification,
} from "../models/types/server.type";
import { createCase } from "../logic/case";
import { CasePayload } from "../models/types/case.type";
import { ErrPayload } from "../models/types/error.type";

function generateIcumoPid(nextCaseId: string) {
    const pidTimeBlock = moment().format("YYYYMMDD_HHmm");
    return pidTimeBlock + "_" + nextCaseId;
}

function buildInsertQuery(
    rec: CasePayload,
    recKeys: string[],
    userId: number,
    nextCaseId: string
): [string, any[]] | [null, null] {
    const colArr: string[] = [];
    const valArr: string[] = [];
    let values: any[] = [];
    let valCount = 0;
    // Generate query values array

    // Column generation business logic
    let colClause = "Generated Column Clause";
    colClause = `(${colClause})`;
    // Value generation business logic here
    let valClause = "Generated value clause";
    valClause = `${valClause}`;
    const queryStr = `INSERT INTO "Case" ${colClause} VALUES ${valClause} RETURNING *`;

    // Add values
    values = values.concat(userId, userId, generateIcumoPid(nextCaseId));
    return [queryStr, values];
}

export async function saveNewCase(
    req: Request,
    res: Response,
    rec: CasePayload,
    userId: number
) {
    const recKeys = Object.keys(rec);
    if (!recKeys || !recKeys.length) {
        getLogger().error("No body payload was sent");
        const e: ErrPayload = { msg: "No body in request found" };
        res.status(400).send(e);
        return;
    }

    // Custom Case ID generation
    const nextCaseId = "";

    const [queryStr, values] = buildInsertQuery(
        rec,
        recKeys,
        userId,
        nextCaseId
    );
    if (queryStr === null || values === null) {
        const e: ErrPayload = {
            msg: "Could not save record, please check the request and try again",
        };
        res.status(400).send(e);
        return;
    }

    const record = await createCase(queryStr, values);
    if (!record) {
        const e: ErrPayload = {
            msg: "Could not create new case record due to conflict to existing case",
        };
        res.status(409).send(e);
        return;
    }

    res.status(201).send({ caseId: record.id });
    const io = req.app.get(SOCKETIO);
    io.emit(SOCKETIO_EVENTS.NEW_CASE, {
        caseId: record.id,
        code: userId,
    } as SocketIO_Notification);
}

// ... Other business logic
