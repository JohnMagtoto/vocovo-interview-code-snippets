// SocketIO name constant
export const SOCKETIO = "socketio";

// SocketIO Events
export enum SOCKETIO_EVENTS {
    NEW_CASE = "NEW_CASE",
    UPDATE_CASE = "UPDATE_CASE",
    DELETE_CASE_IMAGE = "DELETE_CASE_IMAGE",
    // ...
}

export type SocketIO_Notification = {
    caseId: number;
    code: number;
};
