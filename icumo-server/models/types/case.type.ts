import { VehicleSensor } from "./vehicle.type";

/**
 * The case request-response payload structure
 */
export type CasePayload = {
    id?: number;
    reqHosp: number;
    reqHospDept: string;
    reqHospDr: string;
    recHosp: number;
    recHospDept: string;
    recHospDr: string;
    equipment: string;
    vehicleId: number;
    vehicleSensor: VehicleSensor;
    doctors: string;
    nurses: string;
    driver: string;
    cancelReason: string;
    altDestination: string;
    icumoPid?: string;
    reqReason: string;
    afterAdmission: string;
    changeDestination: string;
};

type OrgLocation = {
    // latitude
    x: number;
    // longitude
    y: number;
};

export type RelatedOrgLocPayload = {
    caseId: number;
    reqHosp: number;
    reqHospName: string;
    reqHospLoc: OrgLocation | null;
    recHosp: number;
    recHospName: string;
    recHospLoc: OrgLocation | null;
    garage: number;
    garageName: string;
    garageLoc: OrgLocation | null;
    latestEventType: string;
};

/**
 * Helper function to match dynamic request object keys to known properties
 * @param propKey The property to match
 * @param rec The Case Record
 * @returns
 */
export function getCasePayloadValue(propKey: string, rec: CasePayload) {
    switch (propKey) {
        case "id":
            return rec.id;
        case "reqHosp":
            return rec.reqHosp;
        case "reqHospDept":
            return rec.reqHospDept;
        case "reqHospDr":
            return rec.reqHospDr;
        case "recHosp":
            return rec.recHosp;
        case "recHospDept":
            return rec.recHospDept;
        case "recHospDr":
            return rec.recHospDr;
        case "equipment":
            return rec.equipment;
        case "vehicleId":
            return rec.vehicleId;
        case "vehicleSensor":
            return rec.vehicleSensor;
        case "doctors":
            return rec.doctors;
        case "nurses":
            return rec.nurses;
        case "driver":
            return rec.driver;
        case "cancelReason":
            return rec.cancelReason;
        case "altDestination":
            return rec.altDestination;
        case "icumoPid":
            return rec.icumoPid;
        case "reqReason":
            return rec.reqReason;
        case "afterAdmission":
            return rec.afterAdmission;
        case "changeDestination":
            return rec.changeDestination;
        default:
            throw new Error("Provided property key not supported!");
    }
}

export type CaseImage = {
    imageName: string;
    imageBin: Buffer;
};

export enum CHANGE_DESTINATION_REASON {
    // Transport was manually cancelled
    NOT_TRANSPORT = "NOT_TRANSPORT",
    // Transport was manually changed
    CHANGE_DESTINATION = "CHANGE_DESTINATION",
}

/**
 * The case ID parameter key constant
 * Used in request body, query parameter, and case parameter
 */
export const CASE_ID_PARAM = "case_id";
