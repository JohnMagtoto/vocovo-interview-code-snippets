/**
 * The Collaborator accessible data API routes
 */

import { NextFunction, Request, Response, Router } from "express";
import {
    getDevices,
    getMediaByName,
    getMedicines,
    getMedProcedureCheck,
    getVehicleChecklist,
} from "../../controllers/collab/data.controller";
import { ErrPayload } from "../../models/types/error.type";
import { isUserAuthenticated, isUserAuthorized } from "./helper";

export const collabDataRouter = Router();

/**
 * Check whether request has vehicle parameter or not
 *
 * @param req
 * @param res
 * @param next
 * @returns
 */
async function isVehicleIdPresent(
    req: Request,
    res: Response,
    next: NextFunction
) {
    const query = req.query;
    const vehicleId = query["vehicleId"] as string;
    if (!vehicleId) {
        const msg = "No vehicle ID found. Please include it in the request.";
        const err: ErrPayload = { msg };
        res.status(400).send(err);
        return;
    }
    // Pass the retrieved vehicle ID to the request core process
    res.locals.vehicleId = vehicleId;
    next();
}

collabDataRouter.get(
    "/devices",
    isVehicleIdPresent,
    isUserAuthenticated,
    isUserAuthorized,
    async (req, res) => {
        try {
            const vehicleId: string = res.locals.vehicleId;
            const vId = Number.parseInt(vehicleId);
            await getDevices(res, vId);
        } catch (err) {
            console.log("Internal Server Error", err);
            const e: ErrPayload = {
                msg: "Encountered an unexpected error while fetching devices data",
                detail: "Please contact the developers",
            };
            res.status(500).send(e);
        }
    }
);

/**
 * 薬剤マスタJSON取得API
 */
collabDataRouter.get(
    "/medicines",
    isVehicleIdPresent,
    isUserAuthenticated,
    isUserAuthorized,
    async (req, res) => {
        // Other members' code
    }
);

/**
 * Retrieve the ICU vehicle state checklist items
 */
collabDataRouter.get(
    "/vehicleChecklist",
    isVehicleIdPresent,
    isUserAuthenticated,
    isUserAuthorized,
    async (req, res) => {
        try {
            const vehicleId: string = res.locals.vehicleId;
            const vId = Number.parseInt(vehicleId);

            await getVehicleChecklist(res, vId);
        } catch (err) {
            console.log("Internal Server Error", err);
            const e: ErrPayload = {
                msg: "Encountered an unexpected error while fetching vehicle checklist",
                detail: "Please contact the developers",
            };
            res.status(500).send(e);
        }
    }
);

/**
 * Get a media file saved in CollaboratorMedia using media name
 */
collabDataRouter.get(
    "/media",
    isUserAuthenticated,
    isUserAuthorized,
    async (req, res) => {
        try {
            const { name } = req.query;

            if (!name && typeof name !== "string") {
                const msg = "Please provide correct media name format";
                const err: ErrPayload = { msg };
                res.status(400).send(err);
                return;
            }

            await getMediaByName(res, name as string);
        } catch (err) {
            console.log("Internal Server Error", err);
            const e: ErrPayload = {
                msg: "Encountered an unexpected error while fetching media item",
                detail: "Please contact the developers",
            };
            res.status(500).send(e);
        }
    }
);

/**
 * 処置確認JSONの取得API
 */
collabDataRouter.get(
    "/medcheck",
    isVehicleIdPresent,
    isUserAuthenticated,
    isUserAuthorized,
    async (req, res) => {
        // Other members' code
    }
);
