import { NextFunction, Request, Response } from "express";
import { collabSessionExist, COOKIE_NAME, getCollabUserPermissions } from "../../controllers/collab";
import { UserPermissions } from "../../models/types/auth.type";
import { ErrPayload } from "../../models/types/error.type";

/**
 * Check if the user is allowed access to the server
 * @param req
 * @param res
 * @param next
 */
export async function isUserAuthenticated(req: Request, res: Response, next: NextFunction) {
    const cookies = req.signedCookies;
    const sessionId: number = cookies[COOKIE_NAME];
    if (sessionId !== undefined && (await collabSessionExist(sessionId))) {
        res.locals.sessionId = sessionId;
        next();
    } else {
        const msg = "The current session has expired or is invalid.";
        const detail = "Please log in again and re-try.";
        const err: ErrPayload = { msg, detail };
        res.status(401).send(err);
        return;
    }
}

/**
 * Check if the user is allowed access to resources served by the URL path
 * @param req
 * @param res
 * @param next
 */
export async function isUserAuthorized(req: Request, res: Response, next: NextFunction) {
    const { sessionId } = res.locals;
    if (!sessionId) {
        const msg = "The current session has expired or is invalid.";
        const detail = "Please log in again and re-try.";
        const err: ErrPayload = { msg, detail };
        res.status(401).send(err);
        return;
    }
    const permissions: UserPermissions[] | null = await getCollabUserPermissions(sessionId);
    if (!permissions || !permissions.length) {
        const msg = "The current user has no permissions allocated to it.";
        const detail = "Please use a different user and try again.";
        const err: ErrPayload = { msg, detail };
        res.status(403).send(err);
        return;
    }

    // Check permissions
    if (!permissions.includes(UserPermissions.COLLAB_ACCESS)) {
        res.status(403).send({
            msg: `User is not permitted to access this resource`
        } as ErrPayload);
        return;
    } else {
        // Proceed to next function
        next();
    }
}

/**
 * ユーザーが、RFIDスキャンアプリの使用者として許可されているかを確認する
 * @param req HTTP Request
 * @param res HTTP Response
 * @param next
 */
export async function isScannerUserAllowed(req: Request, res: Response, next: NextFunction) {
    // Other member's code
}