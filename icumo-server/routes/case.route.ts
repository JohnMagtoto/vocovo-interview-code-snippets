import { Request, Response, Router, NextFunction } from "express";
import multer from "multer";
import { COOKIE_NAME, findValidSession } from "../controllers/auth.controller";
import {
    editCase,
    getCaseImage,
    removeCaseImage,
    saveCase,
    saveCaseImage,
} from "../controllers/case.controller";
import { VIRTUAL_TYPE_VEHICLE } from "../../models/db/org.model";
import { ErrPayload } from "../models/types/error.type";
import {
    CaseImage,
    CasePayload,
    CHANGE_DESTINATION_REASON,
    CASE_ID_PARAM,
} from "../models/types/case.type";
import { get1CaseById } from "../logic/case";
import eventRouter from "./event.route";
import { UserSession } from "../../models/db/session.model";
import { getLogger } from "../common";
import { saveNewCase } from "../controller/case.controller";

/**
 * Contains API routes related to case data
 * - ICU-mo Case
 * - ICU-mo Case Image
 * - ICU-mo Case Events
 */
const caseRouter = Router();

// Use in-memory storage for multer instead of actual storage
// NOTE: memory can handl estimated load; upgrade implementation to S3 if load exceeds
//
const storage = multer.memoryStorage();

const fileFilter = (req: any, file: Express.Multer.File, cb: Function) => {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        // Store file
        cb(null, true);
    } else {
        // Reject file
        cb(null, false);
    }
};

// xxxMB file size limit per image
const FILE_SIZE_LIMIT = 10000000;
const upload = multer({
    storage,
    fileFilter,
    limits: {
        // Optional file limits: currently set to
        fileSize: FILE_SIZE_LIMIT,
    },
});

/**
 * Case-related check middleware
 * @param req
 * @param res
 * @param next
 * @returns
 */
async function isSessionValid(req: Request, res: Response, next: NextFunction) {
    const cookies = req.signedCookies;
    const sessionId: number = cookies[COOKIE_NAME];
    const session = await findValidSession(sessionId);
    if (!session) {
        const e: ErrPayload = { msg: "Session not recognized" };
        res.status(401).send(e);
        return;
    }
    // Set session object
    res.locals.caseSession = session;
    next();
}

// Sub-route for case-related events
caseRouter.use(
    "/:case_id/events",
    function (req, res, next) {
        req.body.caseIdStr = req.params.case_id;
        next();
    },
    eventRouter
);

// Create new case route
caseRouter.post("/", isSessionValid, async (req, res) => {
    try {
        const session = res.locals.caseSession;
        if (session.sess_role === VIRTUAL_TYPE_VEHICLE) {
            const e: ErrPayload = { msg: "Vehicle User Session not allowed" };
            res.status(403).send(e);
            return;
        }
        const data: CasePayload = req.body;
        if (data.id) {
            const e: ErrPayload = {
                msg: "New case creation cannot include ID",
            };
            res.status(400).send(e);
            getLogger().error(e.msg);
            return;
        }
        // Verify if request Hospital from payload exists
        if (!data.reqHosp) {
            const e: ErrPayload = {
                msg: "The following data are not included in the request:",
                detail: "[reqHosp]",
            };
            res.status(400).send(e);
            return;
        }
        // Remove ICU-mo Patient ID if sent by client
        //  Value is auto-generated
        if (data.icumoPid) {
            delete data.icumoPid;
        }
        // Determine if correct CHANGE DESTINATION is sent
        if (typeof data.changeDestination !== "undefined") {
            if (
                data.changeDestination !== null &&
                data.changeDestination !==
                    CHANGE_DESTINATION_REASON.NOT_TRANSPORT &&
                data.changeDestination !==
                    CHANGE_DESTINATION_REASON.CHANGE_DESTINATION
            ) {
                const e: ErrPayload = {
                    msg: "Change destination values should be either 'NOT_TRANSPORT' or 'CHANGE_DESTINATION'",
                };
                res.status(400).send(e);
                return;
            }
        }

        // 有効な組織&ユーザーであるか
        // ... Other members' code

        await saveNewCase(req, res, data, session.user_id);
    } catch (err) {
        getLogger().error("Internal Server Error", err);
        res.status(500).send({
            msg: "Encountered an unexpected error during Case creation",
            detail: "Please contact the developers",
        } as ErrPayload);
        return;
    }
});

// Update an existing case
caseRouter.put("/:case_id", isSessionValid, async (req, res) => {
    try {
        const session = res.locals.caseSession;
        const params = req.params;
        const caseId = params[CASE_ID_PARAM];
        if (!caseId) {
            const e: ErrPayload = { msg: "Case ID not recognized" };
            res.status(400).send(e);
            return;
        }
        const caseIdNum = Number.parseInt(caseId);
        const data: CasePayload = req.body;
        if (data.id) {
            const e: ErrPayload = { msg: "Invalid ID property in payload" };
            res.status(400).send(e);
            getLogger().error("Invalid ID property in payload");
            return;
        }
        // Verify if request Hospital from payload exists
        if (!data.reqHosp) {
            const e: ErrPayload = {
                msg: "The following data are not included in the request:",
                detail: "[reqHosp]",
            };
            res.status(400).send(e);
            return;
        }

        // 有効な組織&ユーザーであるか
        // ... Other members' code

        // Patient creation only allowed if case already exists
        const caseData = await get1CaseById(caseIdNum);
        if (!caseData) {
            const e: ErrPayload = {
                msg: `Record for case ID ${caseId} was not found`,
            };
            res.status(403).send(e);
            return;
        }

        // Check if update is allowed with role
        const reqHosp = caseData.req_hosp;
        const recHosp = caseData.rec_hosp;
        const vId = session.vehicle_id;
        if (
            (session.sess_role !== "HQ" &&
                reqHosp &&
                !vId &&
                session.org_id !== reqHosp &&
                session.org_id !== recHosp) ||
            (vId && vId !== caseData.vehicle_id)
        ) {
            const e: ErrPayload = {
                msg: "Current user Organization is not HQ and does not match record ReqHosp or RecHosp or Vehicle",
            };
            res.status(403).send(e);
            return;
        }
        // Remove ICU-mo Patient ID if sent by client
        // Value is auto-generated at creation time so it is
        // not needed here
        if (data.icumoPid) {
            delete data.icumoPid;
        }
        // Determine if correct CHANGE DESTINATION is sent
        if (typeof data.changeDestination !== "undefined") {
            if (
                data.changeDestination !== null &&
                data.changeDestination !==
                    CHANGE_DESTINATION_REASON.NOT_TRANSPORT &&
                data.changeDestination !==
                    CHANGE_DESTINATION_REASON.CHANGE_DESTINATION
            ) {
                const e: ErrPayload = {
                    msg: "Change destination values should be either 'NOT_TRANSPORT' or 'CHANGE_DESTINATION'",
                };
                res.status(400).send(e);
                return;
            }
        }
        await editCase(req, res, caseIdNum, data, session.user_id);
    } catch (err) {
        getLogger().error("Internal Server Error", err);
        res.status(500).send({
            msg: "Encountered an unexpected error during Case modification",
            detail: "Please contact the developers",
        } as ErrPayload);
        return;
    }
});

async function saveImageHandler(
    req: Request,
    res: Response,
    session: UserSession
) {
    try {
        const params = req.params;
        const caseId = params[CASE_ID_PARAM];
        if (!caseId) {
            const e: ErrPayload = { msg: "Case ID not recognized" };
            res.status(400).send(e);
            return;
        }
        const files: Express.Multer.File[] = req.files as Express.Multer.File[];
        if (!files || !files.length) {
            const e: ErrPayload = {
                msg: "No files of type '.jpg' or '.png' recieved",
            };
            res.status(400).send(e);
        }
        const imageFiles: CaseImage[] = [];
        for (const file of files) {
            const imageFile: CaseImage = {
                imageName: file.originalname,
                imageBin: file.buffer,
            };

            imageFiles.push(imageFile);
        }

        const caseIdNum = Number.parseInt(caseId);
        saveCaseImage(res, caseIdNum, session.user_id, imageFiles);
    } catch (err) {
        getLogger().error("Internal Server Error", err);
        res.status(500).send({
            msg: "Encountered an unexpected error during Case Image save process",
            detail: "Please contact the developers",
        } as ErrPayload);
        return;
    }
}

// The Form Data parameter which points to the uploaded images
const IMAGE_FILES_FORM_PARAM = "imageFiles";
caseRouter.post(
    "/:case_id/images",
    upload.array(IMAGE_FILES_FORM_PARAM, 12),
    async (req, res) => {
        const cookies = req.signedCookies;
        const sessionId: number = cookies[COOKIE_NAME];
        const session = await findValidSession(sessionId);
        if (!session) {
            const e: ErrPayload = { msg: "Session not recognized" };
            res.status(401).send(e);
            return;
        }

        // 有効な組織&ユーザーであるか
        // Other members' code

        saveImageHandler(req, res, session);
    }
);

caseRouter.get("/:case_id/images", isSessionValid, async (req, res) => {
    const { imageName } = req.query as { [key: string]: string };
    try {
        const params = req.params;
        const caseId = params[CASE_ID_PARAM];
        if (!caseId) {
            const e: ErrPayload = { msg: "Case ID not recognized" };
            res.status(400).send(e);
            return;
        }

        if (!imageName || !imageName.length) {
            const e: ErrPayload = { msg: "No Case Image name sent" };
            res.status(400).send(e);
            return;
        }

        // 有効な組織&ユーザーであるか
        // Other members' code

        const caseIdNum = Number.parseInt(caseId);
        getCaseImage(res, caseIdNum, imageName);
    } catch (err) {
        getLogger().error("Internal Server Error", err);
        res.status(500).send({
            msg: `Encountered an unexpected error when trying to fetch CaseImage: ${imageName}`,
            detail: "Please contact the developers",
        } as ErrPayload);
        return;
    }
});

caseRouter.delete("/:case_id/images", isSessionValid, async (req, res) => {
    const { imageName } = req.query as { [key: string]: string };
    try {
        const session = res.locals.caseSession;
        const params = req.params;
        const caseId = params[CASE_ID_PARAM];
        if (!caseId) {
            const e: ErrPayload = { msg: "Case ID not recognized" };
            res.status(400).send(e);
            return;
        }

        if (!imageName || !imageName.length) {
            const e: ErrPayload = { msg: "No Case Image name sent" };
            res.status(400).send(e);
            return;
        }

        // 有効な組織&ユーザーであるか
        // Other members' code

        const caseIdNum = Number.parseInt(caseId);
        removeCaseImage(req, res, caseIdNum, session.user_id, imageName);
    } catch (err) {
        getLogger().error("Internal Server Error", err);
        res.status(500).send({
            msg: `Encountered an unexpected error when trying to delete CaseImage: ${imageName}`,
            detail: "Please contact the developers",
        } as ErrPayload);
        return;
    }
});

// Other members' code

export default caseRouter;
