import express, { NextFunction, Request, Response } from "express";
import cookieParser from "cookie-parser";
import cors from "cors";
import { createServer } from "http";
import { Server } from "socket.io";
import { SOCKETIO } from "./models/types/server.type";
import caseRouter from "./routes/case.route";
import { initializeDBPool } from "./logic/db";
import { getLogger } from "./common";
import { collabDataRouter } from "./routes/collab/data.route";

// Setup logger for top-level logging
const logger = getLogger();

logger.info("Initializing ICU-mo server...");

const app = express();

// middleware to use
app.use(express.json()); // parse JSON payload
app.use(cookieParser(process.env.SECRET));

const collabCorsOpts = {
    origin: function (origin: any, callback: Function) {
        // Custom cross origin verification rules
    },
    credentials: true, // Custom cross origin credentials verification
};

// Routes to use
app.use("/api/v1/cases", caseRouter);
// ... Other app API routes

// External APIs to use
// ... Other external routes

// Collaborator APIs to use
// ... Other collaborator routes
app.use("/col/data", cors(collabCorsOpts), collabDataRouter);

// Global error handler
app.use(function (err: Error, req: Request, res: Response, next: NextFunction) {
    logger.error(err.stack);
    res.status(500).send("The server is not able to handle the request");
});

export const httpServer = createServer(app);

// Create Socket IO server instance
const io = new Server(httpServer);
// Set socket io server instance
app.set(SOCKETIO, io);

// Initialize the Postgres DB Pool
initializeDBPool();

logger.info("ICU-mo server initialized.");

export default app;
