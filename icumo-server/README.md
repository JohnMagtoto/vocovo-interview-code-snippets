# ICUMO-server

Server implementation of ICU-mo (ICU Mobile) in NodeJS

Contains the following core snippets

-   base server implementation
-   case route
-   collaborator data route
-   Sample Docker-Compose snippet
