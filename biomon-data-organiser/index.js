var promise = require('bluebird');
var recordFetch = require('./record_fetcher');
var activityVSOrgnaise = require('./activity_vs_organiser');
var NotificationManager = require('./notification_manager');

// ... Other configuration

/**
 * Main function of the application; AWS Lambda looks for this function when it executes code
 *
 * @param event Event object that is sent with the trigger
 * @param context provides runtime information of the Lambda function that is executing
 * @param callback Optional callback to return information to the caller; Otherwise null
 */
exports.handler = function (event, context, callback) {
    var upld_db = promise.promisifyAll(upld_db_pool);
    var monitor_db = promise.promisifyAll(monitor_db_pool);
    var connectionArray = [];

    connectionArray.push(upld_db.getConnectionAsync());
    connectionArray.push(monitor_db.getConnectionAsync());

    var upld_conn = null;
    var monitor_conn = null;

    promise
        .all(connectionArray)
        .then(function (connections) {
            upld_conn = promise.promisifyAll(connections[0]);
            monitor_conn = promise.promisifyAll(connections[1]);
            /**
             * RecordFetcher receives the context object because it has the longest running promises due to
             * the nested queries. The context.done() method will be invoked at the end of the promises
             * in activityOrganiser effectively ending the Lambda execution.
             */
            return recordFetch(
                event.activity_id,
                upld_conn,
                monitor_conn,
                context
            );
        })
        .then(function () {
            return activityVSOrgnaise(
                event.activity_id,
                upld_conn,
                monitor_conn
            );
        })
        .then(function (queryArray) {
            return promise.all(queryArray);
        })
        .then(function () {
            upld_conn.release();
            monitor_conn.release();
            NotificationManager(context, post_options);
        })
        .catch(function (e) {
            if (upld_conn !== null) {
                upld_conn.release();
            }
            if (monitor_conn !== null) {
                monitor_conn.release();
            }
            context.fail();
            throw e;
        });
};
