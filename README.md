# Project Code Snippets

Listed are the code snippets for the latest or most relevant stuff that I did in the last 5-ish years.

A number of the projects are only files with code snippets since I cannot divulge a huge chunk of the code base (proprietary business logic reasons).

Included here are the projects (in chronological order, latest to oldest).

## 🧑🏽‍💻 icumo-server

A mobile ICU app to manage patient transfers.

Included is the server-side part of the application and code pertaining to routes which access the Case resource.

## 🛒 shopping-basket

A front-end code challenge. I did this to make sure I still practice writing regular JS since most of the time, I use TS for work. (Coding without types)

## 🔍 findings-note

A Medical document viewer app. I would like to include the server-side source code for this but it does not use JS (Python + FastAPI) and a huge chunk of business logic was not isolated from the routes part

As such, a code that demonstrates consuption of GraphQL was included as well as use of React Error Boundaries

## 📆 wareki-calendar

A simple NPM package tool that simplifies conversion of Japanese Year (`年`) to English year.

## 🗄 biomon-data-organiser

So far the oldest code in this set, it is a simple code which is run in AWS Lambda. Note that this was written in 2017 so a number of changes could make it better (JS native promises, async, etc.)
