import React, { FC } from "react";
import { FallbackProps } from "react-error-boundary";

const ErrorFallbackComponent: FC<FallbackProps> = ({
    error,
    resetErrorBoundary,
}) => {
    return (
        <div className={`text-center small-text`}>
            <p className={`error-text`}>
                <span className={`error-text bold-text`}>［ERROR］</span>
                <br />
                データを取得できませんでした
            </p>
        </div>
    );
};

export default ErrorFallbackComponent;
