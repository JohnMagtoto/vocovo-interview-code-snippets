import {useState, Suspense, useContext, useEffect} from "react";
import { useHistory } from "react-router-dom";
import Config from "../../commonComponents/Config/Config";
import MainContents from "./components/MainContents";
import styles from "./PatientInfo.module.css";
import { graphql } from "babel-plugin-relay/macro";
import { PatientInfoQuery } from "./__generated__/PatientInfoQuery.graphql";
import Status from "./components/status/Status";
import { useLazyLoadQuery } from "react-relay/hooks";
import { Loading } from "../../commonComponents/loading/Loading";
import { UnauthDialogContext } from "../../contexts/UnauthDialogContext";
import UpdateFlagsContext from "./components/UpdateFlagsContext";
import useUpdateFlags from "./components/UpdateFlagsHook";
import { getAccessToken } from "../../services/auth/authenticator";
import {refreshSession} from "../../services/access/accessor";
import {MeDocPublisher} from "../../services/publisher/medocPublisher";
import {PatientInfoOpenModeEnum} from "../../enums/patientInfoOpenMode.enum";
import {useQuery} from "../../services/router/router";
import MedDocProvider, {MedDocContext} from "./MedDocProvider";
import {ErrorBoundary} from "react-error-boundary";
import ErrorFallbackComponent from "../../commonComponents/fallback/ErrorFallbackComponent";

const gqlQuery = graphql`
    query PatientInfoQuery($patientId: ID!) {
        patient(patientId: $patientId) {
            patientNo
            id
            hospital {
                name
            }
            person {
                personalInfo {
                    gender
                    name
                }
                personRecords {
                    birthInfo {
                        revision
                        birthDatetime
                    }
                }
            }
            status {
                weightChange {
                    timeStamp
                    value
                }
                symptoms {
                    title
                }
                severityChange {
                    timeStamp
                    value
                }
                correctedAge
            }
            comment {
                id
                updatedAt
                note
            }
            impressions {
                id
                updatedAt
                note
            }
            admittedStatus
            answerCode
        }
    }
`;

type PatInfoContentsProps = {
    patientId: string;
    query: URLSearchParams;
    fetchKey: number;
    displayMode: PatientInfoOpenModeEnum;
};

const PatInfoContents = ({
    patientId,
    fetchKey,
    displayMode
}: PatInfoContentsProps): JSX.Element | null => {
    // 表示する患者文書のID
    let { medDocId, setMedDocId } = useContext(MedDocContext);

    const [docFetchKey, setDocFetchKey] = useState<string>(Math.random().toString());

    // Route redirection when no access token found
    const history = useHistory();
    const token = getAccessToken();
    if (!token) {
        history.push("/login");
    }

    const data = useLazyLoadQuery<PatientInfoQuery>(
        gqlQuery,
        {
            patientId: patientId,
        },
        { fetchPolicy: "store-and-network", fetchKey },
    );

    useEffect(() => {
        setDocFetchKey(Math.random().toString());
    }, [patientId, medDocId])

    if (data && data.patient) {
        if (data.patient) {
            return (
                <div>
                    <Config />
                    <Status selected={data.patient} />
                    <ErrorBoundary FallbackComponent={ErrorFallbackComponent}>
                         <MainContents
                            patient={data?.patient}
                            medDocId={medDocId || ""}
                            onChangeMedDocId={(id: string) => {
                                setMedDocId(id);
                            }}
                            displayMode={displayMode}
                            fetchKey={docFetchKey}
                        />
                    </ErrorBoundary>
                </div>
            );
        }
    }

    return (
        <div className={styles["not-found"]}>
            <span>患者が見つかりません。</span>
        </div>
    );
};

const PatientInfo = () => {
    const updateFlags = useUpdateFlags();
    const { fetchKey } = useContext(UnauthDialogContext);

    useEffect(() => {
        // Check session status and refresh if necessary
        refreshSession().then(r => {
            if (r) {
                console.info("User session was refreshed!")
            }
        });
    }, []);

    useEffect(() => {
        // Publish Medical Documents in bulk (if necessary)
        return () =>{
            const publisher = MeDocPublisher.getInstance();
            publisher.publishQueuedDocuments();
        }
    }, [])

    // for get query param
    const query = useQuery();

    // Get the patientId Query Parameter
    const patientId = query.get("patientId");

    // The display mode for PatientInfo screen
    // Defaults to INPUT MODE
    const displayMode = query.get("mode") as PatientInfoOpenModeEnum || PatientInfoOpenModeEnum.INPUT_MODE;

    if (!patientId) {
        return (
            <div className={styles["not-found"]}>
                <span>患者が選択されていません。</span>
            </div>
        );
    }

    return (
        <Suspense
            fallback={
                <Loading text={"患者情報を取得しています"} actionBlock={true} />
            }
        >
            <ErrorBoundary FallbackComponent={ErrorFallbackComponent}>
                <MedDocProvider>
                    <UpdateFlagsContext.Provider value={updateFlags}>
                        <PatInfoContents
                            patientId={patientId}
                            query={query}
                            fetchKey={fetchKey}
                            displayMode={displayMode}
                        />
                    </UpdateFlagsContext.Provider>
                </MedDocProvider>
            </ErrorBoundary>
        </Suspense>
    );
};

export default PatientInfo;
